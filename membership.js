/* membership functions for fuzzy sets */

// Γ function
function T(x, a, b) {
	if(x < a) return 0;
	if(x > b) return 1;
	return (x-a)/(b-a);
}

// S function
function S(x, a, b, g) {
	if(x < a) return 0;
	if(x > g) return 1;
	if(x <= b) return _S(x-a, a, g);
	else return 1-_S(x-g, a, g);
}
function _S(q, a, g) {
	return 2*Math.pow(q/(g-a),2);
}

// L function
function L(x, a, b) {
	return 1-T(x, a, b);
}

// Δ function
function D(x, a, b, g) {
	if(x < a || x > g) return 0;
	if(x <= b) return (x-a)/(b-a);
	return (b-x)/(g-b);
}

// Π function
function P(x, a, g) {
	if(x <= g) return S(x, g-a, g-a/2, g);
	return 1-S(x, g, g+a/2, g+a);
}
