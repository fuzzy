/* simple operators for fuzzy sets:
 * complement (not), intersection (and) and union (and) 
 */

function not_zadeh(x) { return 1-x; }
function not_sugeno(x, l) { return (1-x)/(1+l*x); }
function not_yager(x, l) { return Math.pow(1-Math.pow(x, l), 1/l); }

function not(x) {
	return not_zadeh(x);
	//return not_sugeno(x, -0.5);
	//return not_yager(x, 2);
}

function and_zadeh(a, b) { return Math.min(a, b); }
function and_zadeh2(a, b) { return a*b; }
function and_tnorm(a, b, l) {
	return 1 - Math.min(1, Math.pow(Math.pow(1-a, l) + Math.pow(1-b, l), 1/l));
}

function and(a, b) {
	return and_zadeh(a, b);
	//return and_zadeh2(a, b);
	//return and_tnorm(a, b, 2);
}

function or_zadeh(a, b) { return Math.max(a, b); }
function or_zadeh2(a, b) { return a+b - a*b; }
function or_snorm(a, b, l) {
	return Math.min(1, Math.pow(Math.pow(a, l) + Math.pow(b, l), 1/l));
}

function or(a, b) {
	return or_zadeh(a, b);
	//return or_zadeh(a, b);
	//return or_snorm(a, b, 2);
}
