/* modifier functions for fuzzy sets */

function Dilation(mu) { return PoweredHedge(mu, 0.5); }
var Dil = Dilation;

function Concentration(mu) { return PoweredHedge(mu, 2); }
var Con = Concentration;

function PoweredHedge(mu, exponent){ return Math.pow(mu, exponent); }

function ContrastIntensification(mu){
	if(mu < 0.5) return 2*Math.pow(mu, 2);
	return 1-2*Math.pow((1-mu), 2);
}
var Int = ContrastIntensification;
