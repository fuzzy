/* simple linguistic terms to modify fuzzy sets */

function very(a) { return Con(a); }
function more_or_less(a) { return Dil(a); }
function plus(a) { return PoweredHedge(a, 1.25); }
function slightly(a) { return Int(and(plus(a), not(very(a)))); }
function sort_of(a) {
	return and(Int(more_or_less(a)), Int(more_or_less(not(a))));
}
function pretty(a) { return and(Int(a), not(Int(very(a)))); }
function rather(a) { return Int(very(a)); }
function minus(a) { return PoweredHedge(a, 0.75); }
